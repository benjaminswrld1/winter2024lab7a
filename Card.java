public class Card{
	private String suit;
	private String value;
	
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit(){
		return this.suit;
	}
	public String getValue(){
		return this.value;
	}
	
	public String toString(){
		return this.value + " of " + this.suit;
	}
	public double calculateScore(){
		String[] value = new String[]{"Ace", "two","three","four","five","six","seven","eight","nine","ten","jack","queen","king"};	
		double score = 0.0;
		for(int i = 0; i < value.length;i ++){
			if(value[i].equals(this.value)){				
				score += i+1;
			}
		}
		if(this.suit.equals("heart")){
			score += 0.4;
		}
		if(this.suit.equals("spade")){
			score += 0.3;
		}
		if(this.suit.equals("diamond")){
			score += 0.2;
		}
		if(this.suit.equals("club")){
			score += 0.1;
		}
		return score;
	}
}