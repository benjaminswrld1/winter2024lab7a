import java.util.Random;
public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	//constructor
	public Deck(){
		this.rng = new Random();
		//creates the deck of cards
		this.cards = new Card[52];
		//Sets the number of cards in the deck
		this.numberOfCards = 52;
		String[] suit = new String[]{"spade","heart","club","diamond"};
		String[] value = new String[]{"Ace", "two","three","four","five","six","seven","eight","nine","ten","jack","queen","king"};
		// for loop to make set each card
		int count = 0;
		for(int i = 0; i < suit.length; i++){
			for(int j = 0; j < value.length; j++){
				this.cards[count] = new Card(suit[i],value[j]); 
				count ++;
			}
		}	
	}
	public int length(){
		return this.numberOfCards;
	}
	public Card drawTopCard(){
		this.numberOfCards --;
		return cards[numberOfCards];
	}
	public String toString(){
		String result = "";
		for(int i = 0; i < numberOfCards; i++){
			result = result + this.cards[i] + "\n";
		}
		return result;
	}
	public void shuffle(){
		for(int i = 0; i < numberOfCards; i++){
			int newPosition = rng.nextInt(numberOfCards);
			Card temp = this.cards[i];
			this.cards[i] = this.cards[newPosition];
			this.cards[newPosition] = temp;
		}
	}		
}
	