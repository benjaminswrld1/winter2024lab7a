public class SimpleWar{
	public static void main(String[] args){
		//create deck and shuffle
		Deck deck = new Deck();
		deck.shuffle();
		//both players points 
		int player1 = 0;
		int player2 = 0;
		
		//System.out.println(deck.drawTopCard());
		//System.out.println(deck.drawTopCard());
		while(deck.length() > 0){
			System.out.println("player 1 points: " + player1);
			System.out.println("player 2 points: " + player2);
			
			Card player1Card = deck.drawTopCard();
			System.out.println(player1Card);
			System.out.println(player1Card.calculateScore());
			
			Card player2Card = deck.drawTopCard();
			System.out.println(player2Card);
			System.out.println(player2Card.calculateScore());
			
			
			if(player1Card.calculateScore() < player2Card.calculateScore()){
				player2 ++;
				System.out.println("player 2 wins that one");
			}else{
				player1 ++;
				System.out.println("player 1 wins that one");
			}
			System.out.println("player 1 points: " + player1);
			System.out.println("player 2 points: " + player2);
		}
		if(player1 < player2){
			System.out.println("congrats player 2 you win!!");
		}else{
			System.out.println("congrats player 1 you win!!");
		}
	}
}